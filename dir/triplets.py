# The time complexity of following code is O(N²)
def tripletsZero(array, expectedSum):
    array.sort()
    result = []
    for i in range(len(array) - 2):
        if(i == 0 or (i > 0 and array[i] != array[i-1])):
            left = i+1
            right = len(array) -1
            while (left < right):
                currentSum = array[i] + array[left] + array[right]
                if(currentSum == expectedSum):
                    result.append([array[i],array[left],array[right]])
                    left += 1
                    right -= 1
                elif (currentSum < expectedSum):
                    left += 1
                elif (currentSum > expectedSum):
                    right -= 1
    return result




if __name__ == '__main__':

    result = tripletsZero([-1, 0, 1, 2, -1, -4], 0)
    if(len(result) > 0):
        for i in range(len(result)):
            print(result[i])
    else:
        print("Not Found!")
