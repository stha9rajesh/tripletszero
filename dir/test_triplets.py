import unittest

import dir.triplets as triplets


class TestTriplets(unittest.TestCase):
    def test_triplets1(self):
        self.assertEqual(triplets.tripletsZero([-1, 0, 1, 2, -1, -4],0), [[-1, -1, 2],[-1, 0, 1]])

    def test_triplets2(self):
        self.assertEqual(triplets.tripletsZero([12, 3, 1, 2, -6, 5, 0, -8, -1],0), [[-8, 3, 5],[-6, 1, 5],[-1, 0, 1]])


    def test_triplets4(self):
        self.assertEqual(triplets.tripletsZero([1,1,2,3,-6,6,0,8,10], 0), [[1,2,3],[-6, 0, 6]])





if __name__ == '__main__':
    unittest.main()
